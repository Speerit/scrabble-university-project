#include "BankOfLetters.h"

BankOfLetters::BankOfLetters(Random *rand)
	:rand_(rand)
{
	reset();
}


BankOfLetters::~BankOfLetters()
{
}

char BankOfLetters::getNewLetter()
{
	int index = rand_->random() % amountOfAllLetters;
	while (avaiableLetters[index] == '0')
	{
		++index;
		if (index >= amountOfAllLetters)
			index = 0;
	}

	char result = avaiableLetters[index];
	avaiableLetters[index] = '0';
	if (result == '[')
		return '?';
	return result;
}

void BankOfLetters::returnLetter(char letter)
{
	if (letter == '?')
		letter = '[';
	for (int i = 0; i < amountOfAllLetters; ++i)
	{
		if (avaiableLetters[i] == '0')
		{
			avaiableLetters[i] = letter;
			return;
		}
	}
}

int BankOfLetters::getLetterValue(char letter)
{
	if (letter >= 97 && letter <= 122)
		letter -= 32;

	if (letter - 'A' >= 0 && letter - 'A' < AMOUNT_OF_LETTER_TYPES);
		return letters[(int)(letter - 'A')].points;
	
		return 0;
}

void BankOfLetters::reset()
{
	amountOfAllLetters = 0;
	for (int i = 0; i < AMOUNT_OF_LETTER_TYPES; ++i)
	{
		amountOfAllLetters += letters[i].amount;
	}

	avaiableLetters = new char[amountOfAllLetters];
	int index = 0;
	for (int i = 0; i < AMOUNT_OF_LETTER_TYPES; ++i)
	{
		for (int j = 0; j < letters[i].amount; ++j)
		{
			avaiableLetters[index++] = 'A' + i;
		}
	}
}

int BankOfLetters::getAmountOfLetters()
{
	return amountOfAllLetters;
}

char BankOfLetters::getLetter(int index)
{
	if (index < amountOfAllLetters)
	{
		if (avaiableLetters[index] == '[')
			return '?';
		return avaiableLetters[index];
	}
	return 0;
}

void BankOfLetters::setLetter(int index, char letter)
{
	if (letter == '?')
		letter = '[';

	if (index < amountOfAllLetters)
	{
		avaiableLetters[index] = letter;
	}
}
