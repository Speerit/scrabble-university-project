#pragma once
class Vector2
{
public:
	Vector2();
	Vector2(int x, int y);
	~Vector2();

	Vector2 operator+(Vector2 b)
	{
		return Vector2(this->x + b.x, this->y + b.y);
	}

	int x;
	int y;
};

