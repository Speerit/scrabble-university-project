#pragma once
#include "Vector2.h"

class Window
{
public:
	Window();
	~Window();

	bool draw(char **image, Vector2 size, Vector2 position); //Draw two-dimensional array of chars
	//if color == -1 => color = default
	void drawSymbol(char symbol, int bgColor, int txtColor, Vector2 position);
	//if color == -1 => color = default
	void drawSymbol(char symbol, int bgColor, int txtColor, int posX, int posY);

	void setCursorPos(int posX, int posY);
	void setCursorPos(Vector2 position);
	void setCursorVisibility(bool visible);

private:
	const int DEFAULT_BG_COLOR = 0; // 0 = BLACK
	const int DEFAULT_TXT_COLOR = 15; // 15 = WHITE
};

