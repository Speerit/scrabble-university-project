#include "Window.h"
#include "conio2.h"


Window::Window()
{
}


Window::~Window()
{
}

bool Window::draw(char **image, Vector2 size, Vector2 position)
{
	textcolor(DEFAULT_TXT_COLOR);
	textbackground(DEFAULT_BG_COLOR);

	for (int i = 0; i < size.y; ++i)
	{
		gotoxy(position.x + 1, position.y + 1 + i);
		cputs(image[i]);
	}
	return true;
}

void Window::drawSymbol(char symbol, int bgColor, int txtColor, Vector2 position)
{
	if (bgColor == -1)
		bgColor = DEFAULT_BG_COLOR;
	if (txtColor == -1)
		txtColor = DEFAULT_TXT_COLOR;

	gotoxy(position.x + 1, position.y + 1);
	textcolor(txtColor);
	textbackground(bgColor);
	putch(symbol);
}

void Window::drawSymbol(char symbol, int bgColor, int txtColor, int posX, int posY)
{
	if (bgColor == -1)
		bgColor = DEFAULT_BG_COLOR;
	if (txtColor == -1)
		txtColor = DEFAULT_TXT_COLOR;

	gotoxy(posX + 1, posY + 1);
	textcolor(txtColor);
	textbackground(bgColor);
	putch(symbol);
}

void Window::setCursorPos(int posX, int posY)
{
	gotoxy(posX + 1, posY + 1);
}

void Window::setCursorPos(Vector2 position)
{
	setCursorPos(position.x, position.y);
}

void Window::setCursorVisibility(bool visible)
{
	if(visible)
		_setcursortype(_NORMALCURSOR);
	else
		_setcursortype(_NOCURSOR);
}
