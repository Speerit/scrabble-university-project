#pragma once
#include"conio2.h"
#include"Random.h"
#include"Window.h"
#include"Vector2.h"
#include"Legend.h"
#include"Board.h"
#include"BankOfLetters.h"
#include"AvailableLetters.h"
#include"Console.h"
#include"Points.h"
#include"Dictionary.h"

enum GameState { None, WrittingWord, PlacingWord, SwappingLetters };


class Game
{
public:
	Game();
	~Game();

	bool update();

private:
	Window *win;
	Legend *legend;
	BankOfLetters *bank;
	AvailableLetters *playersLetters;
	AvailableLetters *AILetters;
	Board *board;
	Console *inputField;
	Random *random;
	Points *points_;
	Dictionary *dictionary_;

	Word newWord;
	GameState state;
	bool trainingMode;

	bool loadGame(char *name);
	bool saveGame(char *name);
	void applyWord();
	void moveAI();
	void movePlayer();

	//CONSTANTS -------------------------------------------------------------------
	const int AMOUNT_OF_LETTERS = 7; // aomunt of letters owned by player
};



