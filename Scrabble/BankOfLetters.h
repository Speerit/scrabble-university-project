#pragma once
#include "Random.h"

struct LetterInfo
{
	LetterInfo(int amount, int points)
	{
		this->amount = amount;
		this->points = points;
	}
	int amount;
	int points;
};

class BankOfLetters
{
public:
	BankOfLetters(Random *rand);
	~BankOfLetters();

	char getNewLetter();
	void returnLetter(char letter);
	int getLetterValue(char letter);
	void reset();

	int getAmountOfLetters();
	char getLetter(int index);
	void setLetter(int index, char letter);

private:
	static const int AMOUNT_OF_LETTER_TYPES = 27;

	//First param = amount, second param = points
	const LetterInfo letters[AMOUNT_OF_LETTER_TYPES] = { LetterInfo(9,1),//A
													LetterInfo(2,3),//B
													LetterInfo(2,3),//C
													LetterInfo(4,2),//D
													LetterInfo(12,1),//E
													LetterInfo(2,4),//F
													LetterInfo(3,2),//G
													LetterInfo(2,4),//H
													LetterInfo(9,1),//I
													LetterInfo(1,8),//J
													LetterInfo(1,5),//K
													LetterInfo(4,1),//L
													LetterInfo(2,3),//M
													LetterInfo(6,1),//N
													LetterInfo(8,1),//O
													LetterInfo(2,3),//P
													LetterInfo(1,10),//Q
													LetterInfo(6,1),//R
													LetterInfo(4,1),//S
													LetterInfo(6,1),//T
													LetterInfo(4,1),//U
													LetterInfo(2,4),//V
													LetterInfo(2,4),//W
													LetterInfo(1,8),//X
													LetterInfo(2,4),//Y
													LetterInfo(1,10),//Z
													LetterInfo(10,0)//? - blank
													};
	Random *rand_;
	int amountOfAllLetters;
	char *avaiableLetters;
};





