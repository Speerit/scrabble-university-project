#include "Random.h"
#include <ctime>
#include <cstdlib>

Random::Random()
{
	amountOfCalls_ = 0;
}

Random::~Random()
{
}

unsigned int Random::random()
{
	amountOfCalls_ += 1;
	return rand();
}

void Random::initRand()
{
	seed_ = time(NULL);
	srand(seed_);
	amountOfCalls_ = 0;
}

void Random::initRand(unsigned int seed, int amountOfCalls)
{
	seed_ = seed;
	srand(seed_);
	amountOfCalls_ = amountOfCalls;
	for (int i = 0; i < amountOfCalls_; ++i)
	{
		rand();
	}
}

int Random::getSeed()
{
	return seed_;
}

int Random::getAmountOfCalls()
{
	return amountOfCalls_;
}
