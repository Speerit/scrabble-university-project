#pragma once
#include "Drawable.h"
#include "BankOfLetters.h"
#include "Letter.h"


class AvailableLetters :
	public Drawable
{
public:
	static const int AMOUNT_OF_LETTERS = 7;
	AvailableLetters(Vector2 position, Window *win, BankOfLetters *bank, bool human);
	~AvailableLetters();

	bool swapLetters();
	void swapLetter(int index);
	// if letter avaiable set flag inUse = true
	bool markLetterAsUsed(char letter); 
	void markLetterAsUnused(char letter); // NIEUZYWANE!!!!!
	// remove all letters, earlier marked as inUse = true
	void removeUsedletters();		
	// set in all letters, inUse = false
	void resetUsageOfLetters();			
	bool useLetter(char letter);// DO USUNIECIA!!!
	//reset whole object
	void reset();
	char getLetter(int index);
	void setLetter(int index, char letter);
private:
	static const int LETTER_CHOSEN_BG_COLOR = 2;
	static const int HEIGHT = 4;
	static const int WIDTH = 30;
	const int FIRST_POS = 9; // position of first letter in content array
	const char *CONTENT[WIDTH] ={	"AVAIABLE LETTERS",
									"id:      1  2  3  4  5  6  7",
									"letter:  A  B  C  D  E  F  G",
									"points:  2  4  1  3  1  1  1" };

	Letter letters_[AMOUNT_OF_LETTERS];
	BankOfLetters *bank_;
	bool human_;
	
	// redraw indicated letter
	void updateLetter(int index, bool chosen);	
	// redraw indicated letter with chosen = false;
	void updateLetter(int index);				
	// if letter exists in pool, swap it
	void getNewLetter(int index); 
};

