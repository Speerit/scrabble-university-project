#pragma once
#include"GeneralInfo.h"
#include"CursorInfo.h"
#include"Commands.h"
#include"Window.h"
#include"Vector2.h"

class Legend
{
public:
	Legend(Vector2 position, Window *win);
	~Legend();

	void draw();
	void updateCursorPos(Vector2 newPos);
	void markTrainingMode(bool on);

private:
	Window *win_;
	Vector2 position_;

	GeneralInfo *genInfo_;
	CursorInfo *cursorInfo_;
	Commands *commands_;
};

