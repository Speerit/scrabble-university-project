#include "Commands.h"
#include <string.h>



Commands::Commands(Vector2 position, Window *win)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win)
{
	for (int i = 0; i < size_.y; ++i)
	{
		int j = 0;
		for (; j < strlen(CONTENT[i]) && j < size_.x; ++j)
			image[i][j] = CONTENT[i][j];

		for (; j < size_.x; ++j)
			image[i][j] = ' ';
	}
}

Commands::~Commands()
{
}

void Commands::markTrainingMode(bool on)
{
	if(on)
		drawSymbol(Vector2(0, 13), 2, -1);
	else
		drawSymbol(Vector2(0, 13), -1, -1);

}
