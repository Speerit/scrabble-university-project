#pragma once
#include "Drawable.h"
class GeneralInfo :
	public Drawable
{
public:
	GeneralInfo(Vector2 position, Window *win);
	~GeneralInfo();

private:
	static const int WIDTH = 42;
	static const int HEIGHT = 7;

	const char *CONTENT[WIDTH] = {	" ",
									" Jedrzej",
									" Glowaczewski", 
									" 175592" , 
									" ", 
									" abcdefghijklm",
									" " };

};

