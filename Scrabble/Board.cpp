#include "Board.h"
#include "conio2.h"


Board::Board(Vector2 position, AvailableLetters *playersLetters, AvailableLetters *AILetters, BankOfLetters *bank, Dictionary *dictionary, Window *win)
	:playersLetters_(playersLetters), AILetters_(AILetters), availableLetters_(playersLetters), bank_(bank), dictionary_(dictionary), win_(win), firstWord(true)
{
	position_ = position;

	for (int i = 0; i < BOARD_SIZE; ++i)
		for (int j = 0; j < BOARD_SIZE; ++j)
			multipliers[j][i] = 1;
	
	int center = BOARD_SIZE / 2;
	multipliers[center - 1][center - 1] = 2;
	multipliers[center - 1][center + 1] = 2;
	multipliers[center + 1][center - 1] = 2;
	multipliers[center + 1][center + 1] = 2;

	multipliers[0][3] = 2;
	multipliers[BOARD_SIZE - 1][3] = 2;
	multipliers[0][BOARD_SIZE - 4] = 2;
	multipliers[BOARD_SIZE - 1][BOARD_SIZE - 4] = 2;

	multipliers[3][0] = 2;
	multipliers[3][BOARD_SIZE - 1] = 2;
	multipliers[BOARD_SIZE - 4][0] = 2;
	multipliers[BOARD_SIZE - 4][BOARD_SIZE - 1] = 2;

	int i = 0;
	for (; i < center - 5; ++i)
	{
		multipliers[center - 4 - i][center - i] = 2;
		multipliers[center - 4 - i][center + i] = 2;

		multipliers[center + 4 + i][center - i] = 2;
		multipliers[center + 4 + i][center + i] = 2;

		multipliers[center - i][center - 4 - i] = 2;
		multipliers[center + i][center - 4 - i] = 2;

		multipliers[center - i][center + 4 + i] = 2;
		multipliers[center + i][center + 4 + i] = 2;
	}

		multipliers[center - 4 - i][center - i] = 3;
		multipliers[center - 4 - i][center + i] = 3;
	
		multipliers[center + 4 + i][center - i] = 3;
		multipliers[center + 4 + i][center + i] = 3;

		multipliers[center - i][center - 4 - i] = 3;
		multipliers[center + i][center - 4 - i] = 3;

		multipliers[center - i][center + 4 + i] = 3;
		multipliers[center + i][center + 4 + i] = 3;

		multipliers[center - 2][center - 2] = 3;
		multipliers[center - 2][center + 2] = 3;
		multipliers[center + 2][center - 2] = 3;
		multipliers[center + 2][center + 2] = 3;


		for (int i = 3; i < center; ++i)
		{
			multipliers[center + i][ center + i] = 4;
			multipliers[center + i][center - i] = 4;
			multipliers[center - i][center + i] = 4;
			multipliers[center - i][center - i] = 4;
		}

		multipliers[0][0] = 5;
		multipliers[0][BOARD_SIZE - 1] = 5;
		multipliers[BOARD_SIZE - 1][0] = 5;
		multipliers[BOARD_SIZE - 1][BOARD_SIZE - 1] = 5;

		multipliers[0][center] = 5;
		multipliers[BOARD_SIZE - 1][center] = 5;
		multipliers[center][0] = 5;
		multipliers[center][BOARD_SIZE - 1] = 5;

	reset();
}

void Board::reset()
{
	firstWord = true;
	insertingWord = false;
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			boardFields[j][i] = ' ';
		}
	}
	draw();
}

Vector2 Board::moveCursor(Vector2 shift) // Returns new cursor position
{
	cursorPos = cursorPos + shift;

	if (cursorPos.x < 0)
		cursorPos.x = 0;
	if (cursorPos.x > BOARD_SIZE - 1)
		cursorPos.x = BOARD_SIZE - 1;
	if (cursorPos.y < 0)
		cursorPos.y = 0;
	if (cursorPos.y > BOARD_SIZE - 1)
		cursorPos.y = BOARD_SIZE - 1;

	if (insertingWord)
	{	
		if (newWord->o == Horizontal)
		{
			if (cursorPos.y > BOARD_SIZE - newWord->lenght)
				cursorPos.y = BOARD_SIZE - newWord->lenght;
		}
		else
		{
			if (cursorPos.x > BOARD_SIZE - newWord->lenght)
				cursorPos.x = BOARD_SIZE - newWord->lenght;
		}
	}

	draw();
	return cursorPos;
}

void Board::draw()
{
	//Display board --------------------------------------------------------------
	textbackground(BLACK);
	textcolor(WHITE);

	win_->drawSymbol(218, -1, -1, position_.x, position_.y);
	win_->drawSymbol(192, -1, -1, position_.x, position_.y + BOARD_SIZE + 1);
	win_->drawSymbol(217, -1, -1, position_.x + BOARD_SIZE + 1, position_.y + BOARD_SIZE + 1);
	win_->drawSymbol(191, -1, -1, position_.x + BOARD_SIZE + 1, position_.y);
	for (int i = 1; i < BOARD_SIZE + 1; ++i)
	{
		win_->drawSymbol(196, -1, -1, position_.x + i, position_.y);
		win_->drawSymbol(196, -1, -1, position_.x + i, position_.y + BOARD_SIZE + 1);
	}
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		win_->drawSymbol(179, -1, -1, position_.x, position_.y + i + 1);
		win_->drawSymbol(179, -1, -1, position_.x + BOARD_SIZE + 1, position_.y + i + 1);

		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			displayBoardField(j, i);
		}
	}

	//Display cursor -----------------------------------------------------------------
	if (insertingWord == false)
	{
		displayLetter('*', cursorPos.x, cursorPos.y, OnBoard);
	}
	else //Display new word ---------------------------------------------------------
	{
		int x = cursorPos.x;
		int y = cursorPos.y;
		availableLetters_->resetUsageOfLetters();
		for (int i = 0; i < newWord->lenght; ++i)
		{
			if (boardFields[x][y] != ' ')
			{
				if (boardFields[x][y] == newWord->letters[i])
					displayLetter(newWord->letters[i], x, y, Unnecessary);
				else
					displayLetter(newWord->letters[i], x, y, Unavailable);
			}
			else
			{
				if (availableLetters_->markLetterAsUsed(newWord->letters[i]))
					displayLetter(newWord->letters[i], x, y, Available);
				else
					displayLetter(newWord->letters[i], x, y, Unavailable);
			}
			if (newWord->o == Horizontal) ++y;
			else ++x;
		}
	}


}

char Board::getLetter(Vector2 pos)
{
	return boardFields[pos.x][pos.y];
}

void Board::insertLetter(Vector2 pos, char letter)
{
	boardFields[pos.x][pos.y] = letter;
}

void Board::insertWord(Word * word)
{
	newWord = word;
	insertingWord = true;
	cursorPos.x = newWord->posX;
	cursorPos.y = newWord->posY;

	moveCursor(Vector2(0, 0));
	draw();
}

void Board::changeWordOrientation()
{
	if(newWord->o == Vertical)
		newWord->o = Horizontal;
	else
		newWord->o = Vertical;

	moveCursor(Vector2(0, 0));
	draw();
}

int Board::applyWord()
{
	newWord->posX = cursorPos.x;
	newWord->posY = cursorPos.y;
	int result = getWordPoints(newWord);
	if (result > 0)
	{
		availableLetters_->removeUsedletters();
		int x = cursorPos.x;
		int y = cursorPos.y;
		for (int i = 0; i < newWord->lenght; ++i)
		{
			boardFields[x][y] = newWord->letters[i];
			if (newWord->o) ++y;
			else ++x;
		}
	}
	else
	{
		insertingWord = false;
		draw();
		return -1;
	}
	insertingWord = false;
	firstWord = false;
	draw();
	return result;
}

void Board::turnOnAIMode()
{
	availableLetters_ = AILetters_;
}

void Board::turnOffAIMode()
{
	availableLetters_ = playersLetters_;
}

void Board::addWordByAI(Word * word)
{
	availableLetters_ = AILetters_;
	insertWord(word);
	cursorPos = Vector2(word->posX, word->posY);
	applyWord();
	availableLetters_ = playersLetters_;
}

void Board::cancelWord()
{
	insertingWord = false;
	draw();
}

Word Board::checkIfWordFits(char * word)
{
	int maxPoints, currentPoints;
	maxPoints = -3;
	currentPoints = 0;
	Word bestWord(word);
	bestWord.points = -1;
	Word w(word);
	
	if (w.lenght <= 1)
		return bestWord;

	//check with Horizontal orientation:
	w.o = Horizontal;
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE - w.lenght + 1; ++j)
		{
			w.posX = i;
			w.posY = j;
			currentPoints = getWordPoints(&w);
			if (currentPoints > bestWord.points)
			{
				bestWord = w;
			}
		}
	}

	//check with Vertical orientation:
	w.o = Vertical;
	for (int i = 0; i < BOARD_SIZE - w.lenght + 1; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			w.posX = i;
			w.posY = j;
			currentPoints = getWordPoints(&w);
			if (currentPoints > bestWord.points)
			{
				bestWord = w;
			}
		}
	}
	return bestWord;
}

void Board::writePoints(Console * console)
{
	if (insertingWord == false)
		return;
	newWord->posX = cursorPos.x;
	newWord->posY = cursorPos.y;

	int result = getWordPoints(newWord);
	if (result <= 0)
	{
		console->writeCustomText2("Wrong position!");
		return;
	}

	int result2 = 0;

	char points[BOARD_SIZE * 5 + 8];
	points[0] = '(';
	int x = 1;
	for (int i = 0; i < newWord->lenght; ++i)
	{
		int value = bank_->getLetterValue(newWord->letters[i]);
		result2 += bank_->getLetterValue(newWord->letters[i])*getLetterMultiplier(i);
		if (value < 10)
		{
			points[x++] = '0' + value;
		}
		else
		{
			points[x++] = '0' + value / 10;
			points[x++] = '0' + value % 10;
		}
		points[x++] = '(';
		points[x++] = newWord->letters[i];
		points[x++] = ')';
		if (getLetterMultiplier(i) > 1)
		{
			points[x++] = '*';
			points[x++] = '0' + getLetterMultiplier(i);
		}
		points[x++] = '+';
	}
	points[x - 1] = ')';
	points[x++] = '*';
	int wordMultiplier = result / result2;
	if (wordMultiplier < 10)
	{
		points[x++] = '0' + wordMultiplier;
	}
	else
	{
		points[x++] = '0' + wordMultiplier / 10;
		points[x++] = '0' + wordMultiplier % 10;
	}
	points[x++] = '=';
	if (result < 10)
	{
		points[x++] = '0' + result;
	}
	else
	{
		points[x++] = '0' + result / 10;
		points[x++] = '0' + result % 10;
	}
	points[x++] = '\0';
	console->writeCustomText2(points);
}

void Board::checkFirstWord()
{
	firstWord = true;
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			if (boardFields[j][i] != ' ')
				firstWord = false;
		}
	}
}

int Board::getWordPoints(Word * word)
{
	int wordPoints = 0;
	int wordMultiplier = 1;
	bool positionTaken = true;
	word->points = -1;
	
	int x = word->posX;
	int y = word->posY;

	if (word->o == Vertical)
	{
		if (x > 0 && boardFields[x - 1][y] != ' ')
		{
			return -1; // Wrong position
		}
		if (x < BOARD_SIZE - 1 && boardFields[x + word->lenght][y] != ' ')
		{
			return -1; // Wrong position
		}
	}
	else
	{
		if (y > 0 && boardFields[x][y - 1] != ' ')
		{
			return -1; // Wrong position
		}
		if (y < BOARD_SIZE - 1 && boardFields[x][y + word->lenght] != ' ')
		{
			return -1; // Wrong position
		}
	}

	if (firstWord)
	{
		if (word->o == Horizontal)
		{
			if (!(x == BOARD_SIZE / 2 && y <= BOARD_SIZE / 2 && y + word->lenght > BOARD_SIZE / 2))
				return -1; // Wrong position;
		}
		else
		{
			if (!(y == BOARD_SIZE / 2 && x <= BOARD_SIZE / 2 && x + word->lenght > BOARD_SIZE / 2))
				return -1; // Wrong position;
		}
	}
	else
	{
		bool wrongPosition = true;
		for (int i = 0; i < word->lenght; ++i)
		{
			if (word->o == Horizontal)
			{
				if (boardFields[x][y] != ' ' ||
					(x > 0 && boardFields[x - 1][y] != ' ') ||
					(x < BOARD_SIZE - 1 && boardFields[x + 1][y] != ' '))
				{
					wrongPosition = false;
					break;
				}
			}
			if (word->o == Vertical)
			{
				if (boardFields[x][y] != ' ' ||
					(y > 0 && boardFields[x][y - 1] != ' ') ||
					(y < BOARD_SIZE - 1 && boardFields[x][y + 1] != ' '))
				{
					wrongPosition = false;
					break;
				}
				if (word->o == Horizontal) ++y;
				else ++x;
			}
		}
		if (wrongPosition)
			return -1;
	}

	x = word->posX;
	y = word->posY;

	availableLetters_->resetUsageOfLetters();
	for (int i = 0; i < word->lenght; ++i)
	{
		int result = checkIfLetterFits(x, y, word->letters[i], word->o);
		if (result < 0) // Wrong position
		{
			word->points = -2;
			return word->points;
		}
		else if (result == 0) // place is already taken by the same letter
		{
			if (multipliers[x][y] > 3)  // word multiplier
			{
				wordMultiplier *= multipliers[x][y] - 2;
				wordPoints += bank_->getLetterValue(word->letters[i]);
			}
			else wordPoints += bank_->getLetterValue(word->letters[i])*multipliers[x][y];
		}
		else
		{
			positionTaken = false;
			if (availableLetters_->markLetterAsUsed(word->letters[i]))
			{
				if (multipliers[x][y] > 3)  // word multiplier
				{
					wordMultiplier *= multipliers[x][y] - 2;
					wordPoints += bank_->getLetterValue(word->letters[i]);
				}
				else wordPoints += bank_->getLetterValue(word->letters[i])*multipliers[x][y];
			}
			else
			{
				return -1;	//more letters are required
			}
		}
		if (word->o == Horizontal) ++y;
		else ++x;
	}
	if (positionTaken)
	{
		word->points = 0;
		return word->points;
	}

	word->points = wordPoints * wordMultiplier;
	return word->points;
}

void Board::displayLetter(char letter, int posX, int posY, LetterType type)
{
	int txtcolor;
	if (type == Available)
	{
		txtcolor = LIGHTGREEN;
	}
	else if (type == Unavailable)
	{
		txtcolor = RED;
	}
	else if(type == Unnecessary)
	{
		txtcolor = LIGHTGRAY;
	}
	else
	{
		txtcolor = -1;
	}

	if (posX == BOARD_SIZE / 2 && posX == posY)
	{
		win_->drawSymbol(letter, DARKGRAY, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		return;
	}

	switch (multipliers[posX][posY])
	{
	case 1:
		win_->drawSymbol(letter, -1, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		break;
	case 2:
		win_->drawSymbol(letter, CYAN, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		break;
	case 3:
		win_->drawSymbol(letter, BLUE, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		break;
	case 4:
		win_->drawSymbol(letter, MAGENTA, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		break;
	case 5:
		win_->drawSymbol(letter, BROWN, txtcolor, posX + position_.x + 1, posY + position_.y + 1);
		break;
	}

}

void Board::displayBoardField(int posX, int posY)
{
	displayLetter(boardFields[posX][posY], posX, posY, OnBoard);
}

int Board::checkIfLetterFits(int posX, int posY, char letter, bool orientation)
{
	char *w = new char[BOARD_SIZE + 1];
	if (boardFields[posX][posY] != ' ')
	{
		if (boardFields[posX][posY] != letter)
			return -1; //wrong position
		else
			return 0; //pole zajete przez ta sama litere - nie trzeba nic wiecej sprawdzac
	}
	else
	{
		if (orientation) //Horizontal
		{
			int x = posX;
			boardFields[posX][posY] = letter;
			while (--x > 0 && boardFields[x][posY] != ' ');
			int i = 0;
			while (++x < BOARD_SIZE &&  boardFields[x][posY] != ' ')
			{
				w[i++] = boardFields[x][posY];
			}
			w[i] = '\0';
			boardFields[posX][posY] = ' ';
			if (i > 1 && dictionary_->checkWord(w) == false)
				return -2; //tworzenie nieistniejacego slowa
		}
		else
		{
			int y = posY;
			boardFields[posX][posY] = letter;
			while (--y > 0 && boardFields[posX][y] != ' ');
			int i = 0;
			while (++y < BOARD_SIZE &&  boardFields[posX][y] != ' ')
			{
				w[i++] = boardFields[posX][y];
			}
			w[i] = '\0';
			boardFields[posX][posY] = ' ';
			if (i > 1 && dictionary_->checkWord(w) == false)
				return -2; //tworzenie nieistniejacego slowa
		}
	}

	delete[] w;
	return 1;
}

int Board::getLetterMultiplier(int index)
{
	if (multipliers[cursorPos.x][cursorPos.y] <= 3)
	{
		if(newWord->o == Horizontal)
			return multipliers[cursorPos.x][cursorPos.y + index];
		else
			return multipliers[cursorPos.x + index][cursorPos.y];
	}

	return 1;
}


Board::~Board()
{
}
