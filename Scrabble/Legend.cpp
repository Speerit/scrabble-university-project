#include "Legend.h"
#include "conio2.h"



Legend::Legend(Vector2 position, Window *win)
	:win_(win), position_(position)
{
	genInfo_ = new GeneralInfo(Vector2(0, 0) + position, win);
	cursorInfo_ = new CursorInfo(Vector2(1, 8) + position, win);
	commands_ = new Commands(Vector2(1, 10) + position, win);
}


Legend::~Legend()
{
}

void Legend::draw()
{
	textbackground(DARKGRAY);
	genInfo_->draw();
	commands_->draw();
	textbackground(BLACK);
	cursorInfo_->draw();
}

void Legend::updateCursorPos(Vector2 newPos)
{
	cursorInfo_->update(newPos);
}

void Legend::markTrainingMode(bool on)
{
	commands_->markTrainingMode(on);
}
