#include "Points.h"
#include <string.h>



Points::Points(Vector2 position, Window *win)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win), humanPoints_(0), AIpoints_(0)
{
	for (int i = 0; i < size_.y; ++i)
	{
		int j = 0;
		for (; j < strlen(CONTENT[i]) && j < size_.x; ++j)
			image[i][j] = CONTENT[i][j];

		for (; j < size_.x; ++j)
			image[i][j] = ' ';
	}
	setBorder();
}


Points::~Points()
{
}

int Points::getPoints(Player player)
{
	if (player == Human)
		return humanPoints_;
	else
		return AIpoints_;
}

void Points::setPoints(Player player, int points)
{
	if (player == Human)
		humanPoints_ = points;	
	else
		AIpoints_ = points;

	display();
}

void Points::addPoints(Player player, int points)
{
	if (player == Human)
	{
		humanPoints_ += points;
	}
	else
		AIpoints_ += points;

	display();
}

void Points::reset()
{
	setPoints(Human, 0);
	setPoints(AI, 0);
}

void Points::display()
{
	image[1][9] = ' ';
	image[1][10] = ' ';
	if (humanPoints_ > 99)
		image[1][9] = '0' + humanPoints_ / 100;
	if (humanPoints_ > 9)
		image[1][10] = '0' + (humanPoints_ / 10) % 10;
	image[1][11] = '0' + humanPoints_ % 10;


	image[2][9] = ' ';
	image[2][10] = ' ';
	if (AIpoints_ > 99)
		image[2][9] = '0' + AIpoints_ / 100;
	if (AIpoints_ > 9)
		image[2][10] = '0' + (AIpoints_ / 10) % 10;
	image[2][11] = '0' + AIpoints_ % 10;

	draw();
}
