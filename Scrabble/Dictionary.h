#pragma once
struct WordDic
{
	char *word;
	WordDic *next;

	WordDic()
	{
		next = 0;
	}

};


class Dictionary
{
public:
	Dictionary();
	~Dictionary();

	bool checkWord(char *word); //checks if word exists in dictionary
	WordDic * getAllWords();

private:
	WordDic *words;
	bool error;
};



