#pragma once
class Random
{
public:
	Random();
	~Random();

	unsigned int random();
	void initRand();
	void initRand(unsigned int seed, int amountOfCalls);
	int getSeed();
	int getAmountOfCalls();
private:

	unsigned int seed_;
	int amountOfCalls_;
};