#include "AvailableLetters.h"
#include <string.h>
#include"conio2.h"



AvailableLetters::AvailableLetters(Vector2 position, Window *win, BankOfLetters *bank, bool human)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win), bank_(bank), human_(human)
{
	reset();
}

void AvailableLetters::reset()
{
	for (int i = 0; i < size_.y; ++i)
	{
		int j = 0;
		for (; j < strlen(CONTENT[i]) && j < size_.x; ++j)
			image[i][j] = CONTENT[i][j];

		for (; j < size_.x; ++j)
			image[i][j] = ' ';
	}

	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		getNewLetter(i);
	}
}


AvailableLetters::~AvailableLetters()
{
}

bool AvailableLetters::swapLetters()
{
	bool done = false;
	bool letters[AMOUNT_OF_LETTERS];
	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		letters[i] = false;
	}

	int symbol = getch();
	while (true)
	{
		if (symbol == 0)
		{
			getch();
		}
		else if ((symbol > '0') && (symbol <= '0' + AMOUNT_OF_LETTERS))
		{
			letters[symbol - '1'] = !letters[symbol - '1'];
			updateLetter(symbol - '1', letters[symbol - '1']);
		}
		else if ((symbol == '\r') || (symbol == 'w'))
		{
			for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
				if (letters[i] == true)
					bank_->returnLetter(letters_[i].letter);

			for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
				if (letters[i] == true)
					getNewLetter(i), done = true;
			break;
		}
		symbol = getch();
	}
	return done;
}

void AvailableLetters::swapLetter(int index)
{
	bank_->returnLetter(letters_[index].letter);
	getNewLetter(index);
}

void AvailableLetters::resetUsageOfLetters()
{
	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		letters_[i].inUse = false;
	}
}

bool AvailableLetters::markLetterAsUsed(char letter)
{
	if (letter >= 97 && letter <= 122)
		letter = '?';

	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		if ((letters_[i].letter == letter) && (letters_[i].inUse == false))
		{
			letters_[i].inUse = true;
			return true;
		}
	}
	if (!human_)
	{
		for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
		{
			if ((letters_[i].letter == '?') && (letters_[i].inUse == false))
			{
				letters_[i].inUse = true;
				return true;
			}
		}
	}
	return false;
}

void AvailableLetters::markLetterAsUnused(char letter)
{
	if (letter >= 97 && letter <= 122)
		letter = '?';

	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		if ((letters_[i].letter == letter) && (letters_[i].inUse == true))
		{
			letters_[i].inUse = false;
			return;
		}
	}
	return;
}

void AvailableLetters::removeUsedletters()
{
	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		if (letters_[i].inUse)
		{
			getNewLetter(i);
		}
	}
}

bool AvailableLetters::useLetter(char letter)
{
	if (letter >= 97 && letter <= 122)
		letter = '?';

	for (int i = 0; i < AMOUNT_OF_LETTERS; ++i)
	{
		if (letters_[i].letter == letter)
		{
			getNewLetter(i + 1);
			return true;
		}
	}

	return false;
}


char AvailableLetters::getLetter(int index)
{
	if (index < AMOUNT_OF_LETTERS)
	{
		return letters_[index].letter;
	}
	return 0;
}

void AvailableLetters::setLetter(int index, char letter)
{
	if (index < AMOUNT_OF_LETTERS)
	{
		letters_[index].letter = letter;
		letters_[index].value = bank_->getLetterValue(letters_[index].letter);
		updateLetter(index);
	}
}

void AvailableLetters::updateLetter(int index, bool chosen)
{
	if (letters_[index].letter == '0')
	{
		image[2][FIRST_POS + index * 3] = ' ';
		image[3][FIRST_POS + index * 3] = ' ';
	}
	
	image[2][FIRST_POS + index * 3] = letters_[index].letter;
	if(chosen && human_)
		drawSymbol(Vector2(FIRST_POS + index * 3, 2), LETTER_CHOSEN_BG_COLOR, -1); //do poprawki
	else
		if (human_) drawSymbol(Vector2(FIRST_POS + index * 3, 2));

	int value = letters_[index].value;
	if (value >= 10)
	{
		image[3][FIRST_POS + index * 3] = '1';
		image[3][FIRST_POS + index * 3 + 1] = '0' + value % 10;
		if (human_) drawSymbol(Vector2(FIRST_POS + index * 3, 3));
		if (human_) drawSymbol(Vector2(FIRST_POS + index * 3 + 1, 3));
	}
	else
	{
		image[3][FIRST_POS + index * 3] = '0' + value;
		image[3][FIRST_POS + index * 3 + 1] = ' ';
		if (human_) drawSymbol(Vector2(FIRST_POS + index * 3, 3));
		if (human_) drawSymbol(Vector2(FIRST_POS + index * 3 + 1, 3));
	}
}

void AvailableLetters::updateLetter(int index)
{
	updateLetter(index, false);
}

void AvailableLetters::getNewLetter(int index)
{
	letters_[index].letter = bank_->getNewLetter();
	letters_[index].value = bank_->getLetterValue(letters_[index].letter);
	updateLetter(index);
}
