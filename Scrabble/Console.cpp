#include "Console.h"
#include "conio2.h"
#include <string.h>



Console::Console(Vector2 position, Window *win, AvailableLetters *playersLetters, Dictionary *dictionary)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win), firstLetterPos(2, 2), wordLenght(0), playersLetters_(playersLetters), dictionary_(dictionary)
{
	for (int i = 0; i < HEIGHT; ++i)
	{
		for (int j = 0; j < WIDTH; ++j)
			image[i][j] = ' ';
	}
	setBorder();
	clearConsole();
}


Console::~Console()
{
}

char * Console::getWord(bool trainingMode)
{
	displayStatement("Write word:", 1);
	displayStatement(" ", 2);
	wordLenght = 0;
	int symbol;
	playersLetters_->resetUsageOfLetters();
	win_->setCursorPos(position_ + firstLetterPos);
	win_->setCursorVisibility(true);
	while (true)
	{
		symbol = getch();

		if (symbol == 0)
		{
			getch();
			continue;
		}

		if(symbol >= 'a' && symbol <= 'z')
		{
			symbol -= 32; //conversion on capital
		}
		if ((symbol >= 'A' && symbol <= 'Z') || symbol == '?')
		{
			if (wordLenght && word[wordLenght - 1] == '?')
			{
				if (symbol == '?')
					continue;

				deleteLastLetter();
				symbol += 32;
			}

			if(playersLetters_->markLetterAsUsed(symbol))
				addLetter(symbol, GREEN);
			else
				addLetter(symbol, RED);
			word[wordLenght] = '\0';

			if (trainingMode)
				checkWord();
		}
		else if (symbol == '\r')
		{
			drawSymbol(Vector2(24, 1), -1, -1);
			win_->setCursorVisibility(false);
			if (wordLenght == 0)
				return NULL;
			return word;
		}
		else if (symbol == '\b')
		{
			deleteLastLetter();
			if (trainingMode)
				checkWord();
		}
		else if (symbol == '\x1B')
		{
			win_->setCursorVisibility(false);
			return NULL;
		}
	}
	
}

char * Console::getLoadName()
{
	displayStatement("Load file:", 1);
	displayStatement(" ", 2);
	return getFileName();
}

char * Console::getSaveName()
{
	displayStatement("Save file:", 1);
	displayStatement(" ", 2);
	return getFileName();
}

void Console::writeCustomText(const char text[])
{
	displayStatement(text, 1);
}

void Console::writeCustomText2(const char text[])
{
	displayStatement(text, 2);
}

void Console::writeWrongPosition()
{
	displayStatement("Wrong position.", 1);
	displayStatement(" ", 2);
}

void Console::writeSwap()
{
	displayStatement("Choose letters for swap.", 1);
	displayStatement(" ", 2);
}

void Console::writeWrongWord()
{
	displayStatement("Wrong word.", 1);
	displayStatement(" ", 2);
}

char * Console::getFileName()
{
	wordLenght = 0;
	int symbol;
	win_->setCursorPos(position_ + firstLetterPos);
	win_->setCursorVisibility(true);
	int pos = 0;
	while (true)
	{
		symbol = getch();

		if (symbol == 0)
		{
			getch();
			continue;
		}

		if (symbol >= 33 && symbol <= 125)
		{
			addLetter(symbol, -1);
		}
		else if (symbol == '\r')
		{
			word[wordLenght] = '\0';
			win_->setCursorVisibility(false);
			return word;
		}
		else if (symbol == '\b')
		{
			deleteLastLetter();
		}
		else if (symbol == '\x1B')
		{
			win_->setCursorVisibility(false);
			return NULL;
		}
	}

}

void Console::addLetter(char letter, int bgColor)
{
	if(wordLenght >= MAX_WORD_LENGHT)
		return;

	image[firstLetterPos.y][firstLetterPos.x + wordLenght] = letter;
	win_->drawSymbol(letter, bgColor, -1, position_ + firstLetterPos + Vector2(wordLenght, 0));
	word[wordLenght++] = letter;

}

void Console::deleteLastLetter()
{
	if (wordLenght > 0)
	{
		wordLenght--;
		playersLetters_->resetUsageOfLetters();
		for (int i = 0; i < wordLenght; ++i)
			playersLetters_->markLetterAsUsed(word[i]);

		addLetter(' ', BLACK);
		wordLenght--;
		word[wordLenght] = '\0';
		win_->setCursorPos(position_ + firstLetterPos + Vector2(wordLenght, 0));
	}
}

void Console::clearConsole()
{
	char text[] = "Choose your action.";
	drawSymbol(' ', Vector2(1, 1), -1, -1);
	displayStatement(text, 1);
	displayStatement(" ", 2);
	while (wordLenght)
	{
		deleteLastLetter();
	}

	draw();
}

void Console::displayStatement(const char * text, int line)
{
	if (line != 1 && line != 2)
		line = 1;
	int i = 0;
	for (; i < MAX_STATEMENT_LENGHT; ++i)
	{
		if (text[i] == '\0')
			break;
		image[line][2 + i] = text[i];
	}
	for (; i < MAX_STATEMENT_LENGHT; ++i)
	{
		image[line][2 + i] = ' ';
	}
	draw();
}

void Console::checkWord()
{
	if (dictionary_->checkWord(word))
	{
		drawSymbol(1, Vector2(1, 1), -1, -1);
	}
	else
	{
		drawSymbol(158, Vector2(1, 1), -1, -1);
	}
	win_->setCursorPos(position_ + firstLetterPos + Vector2(wordLenght, 0));
}
