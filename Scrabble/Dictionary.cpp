#define _CRT_SECURE_NO_WARNINGS
#include "Dictionary.h"
#include <stdio.h>
#include <string.h>


Dictionary::Dictionary()
	:error(false)
{
	FILE *dictionary;
	dictionary = fopen("dictionary", "r");
	if (dictionary == NULL)
	{
		error = true;
		return;
	}

	char whiteSymbol, newWord[100];
	words = new WordDic;
	WordDic *currentWord = words;  //do przejrzenia!!!!! zaalecana przebudowa

	while (true)
	{
		if (fscanf(dictionary, "%s", &newWord) <= 0)
		{
			break;
		}
		fscanf(dictionary, "%c", &whiteSymbol);
		WordDic *temp = new WordDic;
		currentWord->next = temp;
		currentWord = temp;
		currentWord->word = new char[strlen(newWord) + 1];
		int i = 0; 
		do
		{
			if (newWord[i] >= 'a' && newWord[i] <= 'z')
			{
				newWord[i] -= 32; //conversion on capital
			}
			currentWord->word[i] = newWord[i];
		} while (newWord[i++] != '\0');
	}
	if (words->next != NULL)
	{
		WordDic *next = words->next;
		delete words;
		words = next;
	}
	else
		error = true;
}


Dictionary::~Dictionary()
{
}

bool Dictionary::checkWord(char * word)
{
	if (error || word[0] == '\0')
		return true;

	int i = -1;

	WordDic *it = words;

	while (it != NULL)
	{
		if (strlen(word) == strlen(it->word))
		{
			int i = 0;
			while (true)
			{
				if (word[i] == '\0')
				{
					return true;
				}
				if (word[i] != it->word[i] && word[i] - 32 != it->word[i])
				{
					break;
				}
				i++;
			}
		}
		it = it->next;
	}

	return false;
}

WordDic * Dictionary::getAllWords()
{
	return words;
}
