#pragma once
#include "Drawable.h"
class CursorInfo :
	public Drawable
{
public:
	CursorInfo(Vector2 position, Window *win);
	~CursorInfo();

	void update(Vector2 cursorPos);

private:
	static const int WIDTH = 40;
	static const int HEIGHT = 1;
	const char *CONTENT = "cursor(A,1)";
};

