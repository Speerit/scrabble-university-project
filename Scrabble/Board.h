#pragma once
#include "Vector2.h"
#include "AvailableLetters.h"
#include "BankOfLetters.h"
#include "Dictionary.h"
#include "Window.h"
#include "Console.h"

enum LetterType { Available, Unavailable, Unnecessary, OnBoard };
enum Orientation {Vertical, Horizontal};
struct Word
{
	char *letters;
	int lenght;
	//word orientation
	Orientation o;
	int posX;
	int posY;
	int points;

	Word(const Word& A)
		:letters(0)
	{
		initiate(A.letters);
		posX = A.posX;
		posY = A.posY;
		points = A.points;
		o = A.o;
	}

	Word()
	:letters(0), o(Vertical) 
	{}
	Word(char *word)
		:letters(0), o(Vertical)
	{
		initiate(word);
	}

	void initiate(char *word)
	{
		delete[] letters;

		int i = 0;
		while (word[i++] != '\0');
		lenght = i;
		letters = new char[lenght];
		for (int i = 0; i < lenght; ++i)
		{
			letters[i] = word[i];
		}
		lenght--; // remove '\0' from lenght
	}

	Word& operator=(const Word &A)
	{
		if (this != &A)
		{
			initiate(A.letters);
			posX = A.posX;
			posY = A.posY;
			points = A.points;
			o = A.o;
		}
		return *this;
	}

	~Word()
	{
			delete[] letters;
	}
};

class Board
{
public:
	static const int BOARD_SIZE = 15;
	Board(Vector2 position, AvailableLetters *playersLetters, AvailableLetters *AILetters, BankOfLetters *bank, Dictionary *dictionary, Window *win);
	~Board();

	Vector2 moveCursor(Vector2 shift); // Returns new cursor position
	void draw();
	char getLetter(Vector2 pos); // used to save game
	void insertLetter(Vector2 pos, char letter);//used to load game
	void insertWord(Word *word);
	void changeWordOrientation();
	int applyWord();
	void turnOnAIMode();
	void turnOffAIMode();
	void addWordByAI(Word *word);
	void cancelWord();
	Word checkIfWordFits(char *word);
	void writePoints(Console *console);
	void checkFirstWord();
	void reset();

private:
	
	static const char BORDER_SYMBOL = '#';

	char boardFields[BOARD_SIZE][BOARD_SIZE];
	int multipliers[BOARD_SIZE][BOARD_SIZE];

	AvailableLetters *availableLetters_;
	AvailableLetters *playersLetters_;
	AvailableLetters *AILetters_;
	BankOfLetters *bank_;
	Dictionary *dictionary_;
	Window *win_;
	Vector2 position_; // Board position
	Vector2 cursorPos;
	Word *newWord;
	Word bestWord;
	bool firstWord;
	bool insertingWord;

	void displayBoardField(int posX, int posY);
	void displayLetter(char letter, int posX, int posY, LetterType type);
	int getWordPoints(Word *word);
	int checkIfLetterFits(int posX, int posY, char letter, bool orientation);
	int getLetterMultiplier(int index);

};

