#pragma once
#include "Drawable.h"
class Commands :
	public Drawable
{
public:
	Commands(Vector2 position, Window *win);
	~Commands();

	void markTrainingMode(bool on);

private:
	static const int WIDTH = 40;
	static const int HEIGHT = 14;

	const char *CONTENT[WIDTH] = {	"CONTROLS:",
									"arrows         - move cursor",
									"q              - quit game",
									"n              - new game",
									"enter          - apply movement",
									"esc            - cancel action",
									"i              - insert new word",
									"backspace      - delete last letter",
									"o              - change orientation",
									"w              - swap letters",
									"1,2,3,4,5,6,7  - choose letters for swap",
									"s              - save game",
									"l              - load game",
									"t              - training mode" };

};

