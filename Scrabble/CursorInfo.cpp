
#define _CRT_SECURE_NO_WARNINGS
#include "CursorInfo.h"
#include <string.h>



CursorInfo::CursorInfo(Vector2 position, Window *win)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win)
{
	strcpy(image[0], CONTENT);
}

CursorInfo::~CursorInfo()
{
}

void CursorInfo::update(Vector2 cursorPos)
{
	image[0][7] = 'A' + cursorPos.x;
	cursorPos.x += 1;
	cursorPos.y += 1;

	if (cursorPos.y < 10)
	{
		image[0][9] = '0' + cursorPos.y;
		image[0][10] = ')';
		image[0][11] = ' ';
	}
	else
	{
		image[0][9] = '1';
		image[0][10] = '0' + cursorPos.y % 10;
		image[0][11] = ')';
	}
	draw();
}
