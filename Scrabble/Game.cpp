#define _CRT_SECURE_NO_WARNINGS
#include "Game.h"
#include <stdio.h>
#include <string.h>



Game::Game()
	:state(None), trainingMode(false)
{
	textbackground(BLACK);
	clrscr();
	textcolor(7);

	random = new Random;
	random->initRand();
	win = new Window();
	legend = new Legend(Vector2(0, 0), win);
	bank = new BankOfLetters(random);
	playersLetters = new AvailableLetters(Vector2(50, 19), win, bank, true);
	AILetters = new AvailableLetters(Vector2(50, 24), win, bank, false);
	dictionary_ = new Dictionary;
	board = new Board(Vector2(50, 0), playersLetters, AILetters, bank, dictionary_, win);
	inputField = new Console(Vector2(0, 25), win, playersLetters, dictionary_);
	points_ = new Points(Vector2(67, 0), win);

	legend->draw();
	playersLetters->draw();
	inputField->draw();
	points_->draw();
}

Game::~Game()
{
}



bool Game::update()
{
	int symbol;
	symbol = getch();
	if ((state == None) && (symbol == 'q'))
		return false;

	switch (symbol)
	{
	case 0:	// move cursor ------------------------------------------
		symbol = getch();		
		if (symbol == 0x48)
			legend->updateCursorPos(board->moveCursor(Vector2(0, -1)));
		else if (symbol == 0x50)
			legend->updateCursorPos(board->moveCursor(Vector2(0, 1)));
		else if (symbol == 0x4b)
			legend->updateCursorPos(board->moveCursor(Vector2(-1, 0)));
		else if (symbol == 0x4d)
			legend->updateCursorPos(board->moveCursor(Vector2(1, 0)));

		if (trainingMode && state == PlacingWord)
		{
			board->writePoints(inputField);
		}
		break;

	case 'i': // writting new word ----------------------------------
		if (state == None)
		{
			char *word = inputField->getWord(trainingMode);
			if (word == NULL)
			{
				inputField->clearConsole();
				break;
			}
			newWord = board->checkIfWordFits(word);
			if (dictionary_->checkWord(word) && newWord.points > 0)
			{
				state = PlacingWord;
				board->insertWord(&newWord);
				if (trainingMode)
				{
					board->writePoints(inputField);
				}
			}
			else
			{
				inputField->clearConsole();
			}
		}
		break;

	case '\r':  // enter pressed ------------------------------------
		if (state == PlacingWord)// apply new word
		{
			state = None;
			inputField->clearConsole();
			applyWord();
			moveAI();
		}
		break;

	case '\x1B':  // esc pressed ------------------------------------
		if (state == PlacingWord)// cancel new word
		{
			state = None;
			board->cancelWord();
			inputField->clearConsole();
		}
		break;

	case 'o': // change word orientation ----------------------------
		if (state == PlacingWord)
		{
			board->changeWordOrientation();
		}
		break;

	case 'w': // swap letters ---------------------------------------
		if (state == None)
		{
			playersLetters->swapLetters();
			moveAI();
		}
		break;

	case 'n': // start new game -------------------------------------
		if (state == None)
		{
			board->reset();
			bank->reset();
			playersLetters->reset();
			AILetters->reset();
			points_->reset();
			random->initRand();
		}
		break;

	case 't': // toggle training mode -------------------------------
		if (state == None)
		{
			trainingMode = !trainingMode;
			legend->markTrainingMode(trainingMode);
		}
		break;

	case 'l': // load existing game ---------------------------------
		if (state == None)
		{
			char *name = inputField->getLoadName();

			if (name)
			{
				loadGame(name);
				legend->draw();
				board->draw();
				playersLetters->draw();
				inputField->draw();
			}
				inputField->clearConsole();
		}
		break;

	case 's': // save game ------------------------------------------
		if (state == None)
		{
			char *name = inputField->getSaveName();
			if(name) saveGame(name);
			inputField->clearConsole();
		}
		break;
	case 'a': // save game ------------------------------------------
		movePlayer();
		break;
	}
	return true;
}

bool Game::loadGame(char *name)
{
	char letter;
	FILE *oldSave;

	oldSave = fopen(name, "r");
	if (oldSave == NULL)
	{
		return false;
	}
	unsigned int seed, amountOfCalls;
	fscanf(oldSave, "%u", &seed);
	fscanf(oldSave, "%c", &letter);
	fscanf(oldSave, "%u", &amountOfCalls);
	random->initRand(seed, amountOfCalls);

	fscanf(oldSave, "%c", &letter);
	for (int i = 0; i < board->BOARD_SIZE; ++i)
	{
		for (int j = 0; j < board->BOARD_SIZE; ++j)
		{
			fscanf(oldSave, "%c", &letter);
			board->insertLetter(Vector2(j, i), letter);
		}
		fscanf(oldSave, "%c", &letter);
	}
	
	for (int i = 0; i < playersLetters->AMOUNT_OF_LETTERS; ++i)
	{
		fscanf(oldSave, "%c", &letter);
		playersLetters->setLetter(i, letter);
	}
	fscanf(oldSave, "%c", &letter);

	for (int i = 0; i < AILetters->AMOUNT_OF_LETTERS; ++i)
	{
		fscanf(oldSave, "%c", &letter);
		AILetters->setLetter(i, letter);
	}
	fscanf(oldSave, "%c", &letter);

	int amount = bank->getAmountOfLetters();
	for (int i = 0; i < amount; ++i)
	{
		fscanf(oldSave, "%c", &letter);
		bank->setLetter(i, letter);
	}
	int p;
	fscanf(oldSave, "%c", &letter);
	fscanf(oldSave, "%i", &p);
	points_->setPoints(Human, p);
	fscanf(oldSave, "%c", &letter);
	fscanf(oldSave, "%i", &p);
	points_->setPoints(AI, p);;

	board->checkFirstWord();

	fclose(oldSave);
	return true;
}

bool Game::saveGame(char *name)
{
	FILE *newSave;

	newSave = fopen(name, "w");
	if (newSave == NULL)
	{
		return false;
	}

	fprintf(newSave, "%u ", random->getSeed());
	fprintf(newSave, "%u\n", random->getAmountOfCalls());
	
	for (int i = 0; i < board->BOARD_SIZE; ++i)
	{
		for (int j = 0; j < board->BOARD_SIZE; ++j)
		{
			fprintf(newSave, "%c", board->getLetter(Vector2(j, i)));
		}
		fprintf(newSave, "\n");
	}

	for (int i = 0; i < playersLetters->AMOUNT_OF_LETTERS; ++i)
	{
		fprintf(newSave, "%c", playersLetters->getLetter(i));
	}
	fprintf(newSave, "\n");

	for (int i = 0; i < AILetters->AMOUNT_OF_LETTERS; ++i)
	{
		fprintf(newSave, "%c", AILetters->getLetter(i));
	}
	fprintf(newSave, "\n");

	int amount = bank->getAmountOfLetters();
	for (int i = 0; i < amount; ++i)
	{
		fprintf(newSave, "%c", bank->getLetter(i));
	}
	fprintf(newSave, "\n");
	fprintf(newSave, "%i", points_->getPoints(Human));
	fprintf(newSave, "\n");
	fprintf(newSave, "%i", points_->getPoints(AI));

	fclose(newSave);
	return true;
}

void Game::applyWord()
{
	int result = board->applyWord(); // information if placing word was succesful
	if (result <= 0)
	{
		inputField->writeWrongPosition();
	}
	if (result > 0)
	{
		points_->addPoints(Human, result);
	}
}

void Game::moveAI()
{
	inputField->writeCustomText("AI moving...");
	Word bestWord;
	Word w;
	bestWord.points = 0;

	WordDic *words = dictionary_->getAllWords();

	int i = 0;
	board->turnOnAIMode();
	while (words)
	{
		if (i++ % 250 == 0)
		{
			int n = i / 250;
			char txt[] = "AI moving... 00.0%";
			txt[16] = '0' + n % 10;
			txt[14] = '0' + n % 100 / 10;
			txt[13] = '0' + n % 1000 / 100;
			inputField->writeCustomText(txt);
		}

		w = board->checkIfWordFits(words->word);
		if (w.points > bestWord.points)
		{
			bestWord = w;
			//break; // some move, not the best one!!!
		}
		words = words->next;
	}
	board->turnOffAIMode();


	if (bestWord.points > 0)
	{
		board->addWordByAI(&bestWord);
		points_->addPoints(AI, bestWord.points);
	}

	int n = random->random() % 7;

	for (int i = 0; i < n; ++i)
	{
		AILetters->swapLetter(random->random() % 7);
	}

	inputField->writeCustomText("AI moved. Choose your action.");
}

void Game::movePlayer()
{
	inputField->writeCustomText("Player moving...");
	Word bestWord;
	Word w;
	bestWord.points = 0;

	WordDic *words = dictionary_->getAllWords();

	int i = 0;
	while (words)
	{
		if (i++ % 250 == 0)
		{
			int n = i / 250;
			char txt[] = "Pl moving... 00.0%";
			txt[16] = '0' + n % 10;
			txt[14] = '0' + n % 100 / 10;
			txt[13] = '0' + n % 1000 / 100;
			inputField->writeCustomText(txt);
		}

		w = board->checkIfWordFits(words->word);
		if (w.points > bestWord.points)
		{
			bestWord = w;
			//break; // some move, not the best one!!!
		}
		words = words->next;
	}


	if (bestWord.points > 0)
	{
		board->insertWord(&bestWord);
		board->applyWord();
		points_->addPoints(Human, bestWord.points);
	}


	inputField->writeCustomText("Player moved.");
	inputField->writeCustomText2("Choose your action.");
}
