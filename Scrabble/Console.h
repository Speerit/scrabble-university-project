#pragma once
#include "Drawable.h"
#include "AvailableLetters.h"
#include "Dictionary.h"
class Console :
	public Drawable
{
public:
	Console(Vector2 position, Window *win, AvailableLetters *playersLetters, Dictionary *dictionary);
	~Console();

	char* getWord(bool trainingMode); // brak hermetycznosci
	char* getLoadName();
	char* getSaveName();
	//writes text in first line
	void writeCustomText(const char text[]);
	//writes text in second line
	void writeCustomText2(const char text[]);
	void writeWrongPosition();
	void writeSwap();
	void writeWrongWord();
	void deleteLastLetter();
	void clearConsole();

private:
	static const int WIDTH = 90; //Width of CONTENT + '\0'
	static const int HEIGHT = 4;
	static const int MAX_WORD_LENGHT = 15;
	static const int MAX_STATEMENT_LENGHT = 86;

	AvailableLetters *playersLetters_;
	Dictionary *dictionary_;
	char word[MAX_WORD_LENGHT + 1];
	Vector2 firstLetterPos;
	int wordLenght;

	void addLetter(char letter, int bgColor);
	char* getFileName();
	void displayStatement(const char *text, int line);
	void checkWord();
};

