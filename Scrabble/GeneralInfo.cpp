#include "GeneralInfo.h"
#include <string.h>


GeneralInfo::GeneralInfo(Vector2 position, Window *win)
	:Drawable(Vector2(WIDTH, HEIGHT), position, win)
{ 
	int freeSpace = 0;

	for (int i = 0; i < size_.y; ++i)
	{
		int j = 0;
		for (; j < strlen(CONTENT[i]) && j <  size_.x; ++j)
			image[i][j] = CONTENT[i][j];

		for (; j < size_.x; ++j)
			image[i][j] = ' ';
	}
	setBorder();
}


GeneralInfo::~GeneralInfo()
{
}
