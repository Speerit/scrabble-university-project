#pragma once
#include "Drawable.h"

enum Player {Human, AI};

class Points :
	public Drawable
{
public:
	Points(Vector2 position, Window *win);
	~Points();

	int getPoints(Player player);
	void setPoints(Player player, int points);
	void addPoints(Player player, int points);
	void reset();

private:
	static const int WIDTH = 14; //Width of CONTENT + '\0'
	static const int HEIGHT = 4;

	const char *CONTENT[WIDTH] = {	"##############",
									"# Human:   0 #",
									"# AI:      0 #",
									"##############" };

	int humanPoints_;
	int AIpoints_;
	void display();
};

