#pragma once
#include "Vector2.h"
#include "Window.h"

class Drawable
{
public:
	Drawable(Vector2 size, Vector2 position, Window *win);
	~Drawable();

	void draw();

private:

protected:
	Window *win_;
	char **image;
	Vector2 size_;
	Vector2 position_;

	// if color == -1 => color = default
	void drawSymbol(char symbol, Vector2 position, int bgColor, int txtColor);
	void drawSymbol(Vector2 position, int bgColor, int txtColor);
	void drawSymbol(Vector2 pos);
	void setBorder();
};

