#include "Drawable.h"



Drawable::Drawable(Vector2 size, Vector2 position, Window *win)
	:size_(size), position_(position), win_(win)
{
	image = new char*[size.y];
	for (int i = 0; i < size.y; ++i)
	{
		image[i] = new char[size.x + 1];
		for (int j = 0; j < size.x; ++j)
		{
			image[i][j] = ' ';

		}
		image[i][size_.x] = '\0';
	}
}


Drawable::~Drawable()
{
}

void Drawable::draw()
{
	win_->draw(image, size_, position_);
}

void Drawable::drawSymbol(char symbol, Vector2 pos, int bgColor, int txtColor)
{
	win_->drawSymbol(symbol, bgColor, txtColor, pos + position_);
}

void Drawable::drawSymbol(Vector2 pos, int bgColor, int txtColor)
{
	win_->drawSymbol(image[pos.y][pos.x], bgColor, txtColor, pos + position_);
}

void Drawable::drawSymbol(Vector2 pos)
{
	drawSymbol(pos, -1, -1);
}

void Drawable::setBorder()
{
	for (int i = 1; i < size_.x - 1; ++i)
	{
		image[0][i] = 196;
		image[size_.y - 1][i] = 196;
	}
	for (int i = 1; i < size_.y - 1; ++i)
	{
		image[i][0] = 179;
		image[i][size_.x - 1] = 179;
	}
	image[0][0] = 218;
	image[0][size_.x - 1] = 191;
	image[size_.y - 1][size_.x - 1] = 217;
	image[size_.y - 1][0] = 192;
}
